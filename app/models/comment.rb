class Comment < ActiveRecord::Base
	belongs_to :post
	validates_present_of :post_id
	validates_present_of :body
end
